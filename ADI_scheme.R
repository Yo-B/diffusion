#    {ADI_scheme, a solver of diffusion terms by finite volume method and ADI scheme}
#    Copyright (C) {2017}  {Yoann Bourhis} bourhis.yoann@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.


#=====================================================================
#-----------      2D diffusion semi-implicit scheme  :    ------------
#-----------        Alternating Direction Implicit       -------------
#-----------                30/04/2015                   -------------
#                                    by Yoann Bourhis & Youcef Mammeri
#=====================================================================

library(Matrix)
library(raster)
library(spatstat)

#=====================================================================
#-----------     Sparse Banded matrices generation       -------------
#=====================================================================

gen.adi.matrix <- function(dt = dt, # time step
                           D=DifMap, # Diffusion coefficient mapping (matrix w/ same dim as pop. matrix)
                           BC='neumann'){ # 'neumann' 'dirichlet' boundary condition for the diffusive part
  
  
  NxA <- nrow(D) ; dx <- 1/NxA
  NyA <- ncol(D) ; dy <- 1/NyA
  
  I <- bandSparse(n=(NxA)*(NyA),k=0, 
                  diagonal=list(rep(1,(NxA)*(NyA))))
  
  # multiplying coefficient for the principal diagonal boundary-related cells
  if (BC=='dirichlet'){
    bc <- 2
  }else if (BC=='neumann'){
    bc <- 1
  } 
  
  #     1st matrix generation
  # ____________________________________________________
  
  a <- b <- c <- matrix(0, NyA, NxA)
  for (j in 1:(NyA)){ 
    for (i in 2:(NxA-1)){   
      a[j,i] <- -dt/dx**2*(0.5*(D[i+1,j]+D[i,j]) + 0.5*(D[i-1,j]+D[i,j]))
      
      b[j,i] <- dt/dx**2*(0.5*(D[i+1,j]+D[i,j]))
      
      c[j,i] <- dt/dx**2*(0.5*(D[i-1,j]+D[i,j]))
      
    }
    b[j,1] <- dt/dx**2*(0.5*(D[2,j]+D[1,j]))
    c[j,NxA] <- dt/dx**2*(0.5*(D[NxA-1,j]+D[NxA,j]))
    
    a[j,1] <- -dt/dx**2*(0.5*(D[2,j]+D[1,j])) * bc
    a[j,NxA] <- -dt/dx**2*(0.5*(D[NxA-1,j]+D[NxA,j])) * bc
    
  }
  
  a <- as.vector(t(a)) ; b <- as.vector(t(b)) ; c <- as.vector(t(c))
  Axx <- bandSparse(n=(NxA)*(NyA),k=c(0,1,-1), 
                    diagonal=list(a,b[-length(b)], c[-1]))
  
  
  #     2nd matrix generation 
  # ____________________________________________________
  
  a <- b <- c <- matrix(0, NxA, NyA)
  for (i in 1:(NxA)){ 
    for (j in 2:(NyA-1)){   
      a[i,j] <- -dt/dy**2*(0.5*(D[i,j+1]+D[i,j]) + 0.5*(D[i,j-1]+D[i,j]))
      
      b[i,j] <- dt/dy**2*(0.5*(D[i,j+1]+D[i,j]))
      
      c[i,j] <- dt/dy**2*(0.5*(D[i,j-1]+D[i,j]))
      
    }
    b[i,1] <- dt/dy**2*(0.5*(D[i,2]+D[i,1]))
    c[i,NyA] <- dt/dy**2*(0.5*(D[i,NyA-1]+D[i,NyA]))
    
    a[i,1] <- -dt/dy**2*(0.5*(D[i,2]+D[i,1])) * bc
    a[i,NyA] <- -dt/dy**2*(0.5*(D[i,NyA-1]+D[i,NyA])) * bc
  }
  
  a <- as.vector(a) ; b <- as.vector(b) ; c <- as.vector(c)
  Ayy <- bandSparse(n=(NxA)*(NyA),k=c(0, NxA, -(NxA)), 
                    diagonal=list(a, 
                                  b[-(((NxA)*(NyA-1)+1):((NxA)*(NyA)))], 
                                  c[-(1:(NxA))]))
  
  Axx1 <- (I-0.5*Axx) 
  Axx2 <- (I+0.5*Axx) 
  Ayy1 <- (I-0.5*Ayy)
  Ayy2 <- (I+0.5*Ayy)
  
  #     Advective matrices generation for fokker-planck
  # ____________________________________________________
  mu.p.x <- -(rbind(D[-1,], D[NxA,]) - D) / dx
  mu.m.x <- -(D - rbind(D[1,], D[-NxA,])) / dx
  mu.p.y <- -(cbind(D[,-1], D[,NyA]) - D) / dy
  mu.m.y <- -(D - cbind(D[,1], D[,-NyA])) / dy
  
  list(Axx1=Axx1, Axx2=Axx2, Ayy1=Ayy1, Ayy2=Ayy2, mu.p.x=mu.p.x,
       mu.m.x=mu.m.x, mu.p.y=mu.p.y, mu.m.y=mu.m.y)
}

#=============================================================
#--------     Advective step of Fokker-Planck      -----------
#=============================================================

adv.fp <- function(species, nx, ny, mu.p.x, mu.m.x, mu.p.y, mu.m.y, dx, dy, dt, it.cfl){
  
  for (i in 1:it.cfl){
    F.p.x <- pmin(mu.p.x, 0) * rbind(species[-1,], 0) + pmax(mu.p.x, 0) * species
    F.m.x <- pmin(mu.m.x, 0) * species + pmax(mu.m.x, 0) * rbind(0, species[-nx, ]) 
    F.p.y <- pmin(mu.p.y, 0) * cbind(species[,-1], 0) + pmax(mu.p.y, 0) * species
    F.m.y <- pmin(mu.m.y, 0) * species + pmax(mu.m.y, 0) * cbind(0, species[, -ny]) 
    
    species <- species - (dt/it.cfl)/dx*(F.p.x - F.m.x) - (dt/it.cfl)/dy*(F.p.y - F.m.y)
    
  }
  species
}



#=============================================================
#-----------     Diffusion solving function      ------------
#=============================================================

diff.fick <- function(species, nx, ny, Axx1=Axx1, Axx2=Axx2, Ayy1=Ayy1, Ayy2=Ayy2){
  # solving the Diffusion with ADI: 2 half timestep alternating explicit and implicit dimensions solving
  u1 <- as.vector(species)
  
  u2 <- solve(Axx1 , Ayy2 %*% u1) # explicit Y, implicit X  
  u3 <- solve(Ayy1 , Axx2 %*% u2) # explicit X, implicit Y
  
  species <- matrix(u3, nx, ny)
  
  species
}




#   other ..
# ____________________________
padding.map <- function(map, vpad=0, hpad=0, val=0){
  if (hpad > 0){
    map <- cbind(matrix(val, nrow=nrow(map), ncol=hpad),
                 map,
                 matrix(val, nrow=nrow(map), ncol=hpad))
  }
  if (vpad > 0){
    map <- rbind(matrix(val, ncol=ncol(map), nrow=vpad),
                 map,
                 matrix(val, ncol=ncol(map), nrow=vpad))
  }
  map
}


#======================================
#-----------    CALLS     -------------
#======================================



#     exp 1, low diffusion field boundaries
# _________________________________________________________

Nx <- 100 ; dx <- 1/Nx
Ny <- 100 ; dy <- 1/Ny
dt <- 0.01 ; t.end <- 5
CFL <- 1 # seems robust even for higher CFL
# difusion coefficients
ld <- 0.01 ; hd <- 0.05 

# initial population density
density <- matrix(0, Nx, Ny) ; density[45:55,45:55] <- 1

# diffusion mapping
DifMap <- matrix(hd, Nx, Ny)
DifMap[c(25:30, 70:75),] <- ld
DifMap[,c(25:30, 70:75)] <- ld
# a bit of smoothing to avoid irregularities at the begining of simulations
DifMap <- padding.map(DifMap, 3,3,val=hd)
DifMap <- as.matrix(blur(as.im(DifMap), sigma=1))
DifMap[DifMap<0] <- 0
DifMap <- DifMap[4:(Nx+3), 4:(Ny+3)]

# generate ADI matrices
ADImatrices <- gen.adi.matrix(dt, DifMap, BC='neumann')
invisible(mapply(assign, names(ADImatrices), ADImatrices, MoreArgs = list(pos = 1)))
it.cfl <- max(1, ceiling(max(abs(c(mu.p.x, mu.m.x)) * (dt) / dx, abs(c(mu.p.y, mu.m.y)) * (dt) / dy)) / CFL) # CFL compliant time step

# loop w/ display
x11()
plot(raster(density), asp=1);cat('\n', sum(density))
for (i in seq(0, t.end, by=dt)){
  
  # fick diffusion (first part of the Fokker-Planck diffusion)
  density <- diff.fick(density, nx=Nx, ny=Ny, Axx1=Axx1, Axx2=Axx2, Ayy1=Ayy1, Ayy2=Ayy2)
  # advective part of the Fokker-Planck diffusion (if commented, Fick's applies)
  density <- adv.fp(density, nx=Nx, ny=Ny, mu.p.x=mu.p.x, mu.m.x=mu.m.x, mu.p.y=mu.p.y,
                    mu.m.y=mu.m.y, it.cfl=it.cfl, dx=dx, dy=dy, dt=dt)
  
  par(mfrow=c(2,1)); plot(raster(density), asp=Nx/Ny); plot(density[40,], type='l', ylim=c(0,.2)); lines(density[,40], col='red')
  cat('\nt=',i,'/', t.end,'    sum(density) =', sum(density))
  
}



#     exp 2, high diffusion field boundaries
# _________________________________________________________

Nx <- 100 ; dx <- 1/Nx
Ny <- 100 ; dy <- 1/Ny
dt <- 0.01 ; t.end <- 5
CFL <- 1 # seems robust even for higher CFL
# difusion coefficients
ld <- 0.01 ; hd <- 0.05 # inverse to test high diffusion boundaries

# initial population density
density <- matrix(0, Nx, Ny) ; density[45:55,45:55] <- 1

# diffusion mapping
DifMap <- matrix(bd, Nx, Ny)
DifMap[c(25:30, 70:75),] <- hd
DifMap[,c(25:30, 70:75)] <- hd
# a bit of smoothing to avoid irregularities at the begining of simulations
DifMap <- padding.map(DifMap, 3,3,val=bd)
DifMap <- as.matrix(blur(as.im(DifMap), sigma=1))
DifMap[DifMap<0] <- 0
DifMap <- DifMap[4:(Nx+3), 4:(Ny+3)]

# generate ADI matrices
ADImatrices <- gen.adi.matrix(dt, DifMap, BC='neumann')
invisible(mapply(assign, names(ADImatrices), ADImatrices, MoreArgs = list(pos = 1)))
it.cfl <- max(1, ceiling(max(abs(c(mu.p.x, mu.m.x)) * (dt) / dx, abs(c(mu.p.y, mu.m.y)) * (dt) / dy)) / CFL) # CFL compliant time step

# loop w/ display
x11()
plot(raster(density), asp=1);cat('\n', sum(density))
for (i in seq(0, t.end, by=dt)){
  
  # fick diffusion (first part of the Fokker-Planck diffusion)
  density <- diff.fick(density, nx=Nx, ny=Ny, Axx1=Axx1, Axx2=Axx2, Ayy1=Ayy1, Ayy2=Ayy2)
  # advective part of the Fokker-Planck diffusion (if commented, Fick's applies)
  density <- adv.fp(density, nx=Nx, ny=Ny, mu.p.x=mu.p.x, mu.m.x=mu.m.x, mu.p.y=mu.p.y,
                    mu.m.y=mu.m.y, it.cfl=it.cfl, dx=dx, dy=dy, dt=dt)
  
  par(mfrow=c(2,1)); plot(raster(density), asp=Nx/Ny); plot(density[40,], type='l', ylim=c(0,.2)); lines(density[,40], col='red')
  cat('\nt=',i,'/', t.end,'    sum(density) =', sum(density))
  
}
